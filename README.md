# README #

This is an implementation of the [nginx HTTP authentication server protocol](http://nginx.org/en/docs/mail/ngx_mail_auth_http_module.html#protocol). It implements a whitelist module that uses ip.txt, subnet.txt and host.txt files.

### What is this repo for? ###

* nginx HTTP authentication server implementation
* Version 0.1
* [nginx mail auth HTTP module](http://nginx.org/en/docs/mail/ngx_mail_auth_http_module.html)

### How do I get set up? ###

* set up nginx with something similar to the sample template
* create `ip.txt`, `subnet.txt`, and `host.txt` files
* run `python main.py` or `uwsgi --http-socket :8008 --plugin python --wsgi-file main.py`

### Who do I talk to? ###

* Use the Issue Tracker!