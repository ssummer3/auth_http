import logging
import socket


import ipaddress


log = logging.getLogger(__name__)

hosts = {__.strip() for __ in open('host.txt', 'r')}
ips = {
    ipaddress.ip_address(__.strip().decode())
    for __ in open('ip.txt', 'r')
}
subnets = {
    ipaddress.ip_network(__.strip().decode())
    for __ in open('subnet.txt', 'r')
}


def check(client):
    return ip(client['ip']) or host(client['host'])


def ip(client):
    try:
        client = ipaddress.ip_address(client)
    except ipaddress.AddressValueError:
        client = ipaddress.ip_address(client.decode())
    if client in ips:
        log.info('{} found in ips'.format(client))
    elif any(client in subnet for subnet in subnets):
        log.info('{} found in subnets'.format(client))
    else:
        log.info('{} not found'.format(client))
        return False
    return True


def host(client):
    if client.lower() in hosts:
        log.info('{} found in hosts'.format(client))
        return True
    else:
        try:
            client_ip = socket.gethostbyname(client)
            log.info('{} not found in hosts, trying {}'.format(client, client_ip))
        except socket.gaierror:
            log.info('{} not found in hosts, DNS failed'.format(client))
            return False
        return ip(client_ip)
