import logging
import os
import socket


log = logging.getLogger(__name__)

success = [('Auth-Status', 'OK')]
failure = [
     ('Auth-Error-Code', '535 5.7.0'),
     ('Auth-Wait', '10'),
]


def validate(auth_key):
    if auth_key == os.environ.get('AUTH_KEY'):
        log.info('Server connection validated.')
        return True
    log.info('Server connection NOT validated.')
    return False


def upstream(smtp_server='smtp-relay.gmail.com', smtp_port='587'):
    server = socket.gethostbyname(smtp_server)
    return success + [
        ('Auth-Server', server),
        ('Auth-Port', smtp_port),
    ]


def fail():
    return failure + [
        ('Auth-Status', 'client not recognized, buh bye')
    ]
