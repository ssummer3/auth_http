import logging

import authserver
import whitelist


logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)
log = logging.getLogger(__name__)


LOGLINE = '{remote_ip}: {ip}/{host} HELO {helo} {from} {to} {attempt}'
DEFAULT_HEADERS = [('Content-Type', 'text/plain')]


def application(env, resp):
    log.debug('ENV: {}'.format(env))
    headers = {
        k[5:]: v
        for k, v in env.items()
        if k.startswith('HTTP_')
    }
    log.debug('HEADERS: {}'.format(headers))

    if not authserver.validate(headers.get('X_AUTH_KEY')):
        resp('403 Forbidden', DEFAULT_HEADERS)
        return ['nope']

    client = {
        'remote_ip': env.get('REMOTE_ADDR'),
        'ip': headers.get('CLIENT_IP', '0.0.0.0'),
        'host': headers.get('CLIENT_HOST', '[UNKNOWN]'),
        'helo': headers.get('AUTH_SMTP_HELO'),
        'from': headers.get('AUTH_SMTP_FROM'),
        'to': headers.get('AUTH_SMTP_TO'),
        'attempt': headers.get('AUTH_LOGIN_ATTEMPT'),
    }
    log.info(LOGLINE.format(**client))

    response_headers = authserver.upstream() \
        if whitelist.check(client) \
        else authserver.fail()

    resp('200 OK', response_headers)
    return []


if __name__ == '__main__':
    from wsgiref.simple_server import make_server

    # srv = make_server('127.0.0.1', 8008, application)
    srv = make_server('', 8008, application)
    srv.serve_forever()
